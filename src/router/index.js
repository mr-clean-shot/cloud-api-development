import Vue    from 'vue'
import Router from 'vue-router'

import Login from './../views/Login.vue'
import Secure from './../views/Secure.vue'
import UserList from './../components/UserList.vue'
import UserProfile from './../components/UserProfile.vue'
Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            redirect: {
                name: 'login'
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/secure',
            name: 'secure',
            component: Secure
        }
    ],
    linkActiveClass: 'active',
    mode: 'history'
})
